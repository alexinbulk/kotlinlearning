package com.test.alexander.test

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : Activity() {

    private var text: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var button: Button = findViewById(R.id.buttonAbout)
        val aboutIntent = Intent(this, AboutActivity::class.java)

       button.setOnClickListener(View.OnClickListener {
            startActivity(aboutIntent)
        })

        //    setContentView()
        //  var text = findViewById<TextView>(R.id.textView)
        //      text.setText(getString(R.string.action_settings))

        //    text?.setText("Hi!")
    }
}
